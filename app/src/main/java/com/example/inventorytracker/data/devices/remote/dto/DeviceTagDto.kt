package com.example.inventorytracker.data.devices.remote.dto

import com.example.inventorytracker.domain.devices.model.DeviceTag
import com.google.gson.annotations.SerializedName

data class DeviceTagDto(
    @SerializedName("absense")
    val absence: Int,
    val active: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("deleted_at")
    val deletedAt: Any,
    @SerializedName("device_type")
    val deviceType: Int,
    val hash: String,
    val id: Int,
    @SerializedName("took_at")
    val tookAt: String
)

fun DeviceTagDto.toDeviceTag()
    = DeviceTag(
        absence = absence,
        active = active,
        deviceType = deviceType,
        hash = hash,
        tookAt = tookAt
    )