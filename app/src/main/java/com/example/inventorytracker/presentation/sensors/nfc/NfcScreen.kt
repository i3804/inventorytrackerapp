package com.example.inventorytracker.presentation.sensors.nfc

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.inventorytracker.presentation.sensors.nfc.components.AddTagButton
import com.example.inventorytracker.presentation.sensors.nfc.components.UpdateTagButton
import com.ramcosta.composedestinations.annotation.Destination

@OptIn(ExperimentalMaterial3Api::class)
@Composable
@Destination
fun NfcScreen(
    nfcTagViewModel: NfcTagViewModel = hiltViewModel()
//    val addTagRepository
//    val updateTagRepository
) {
    val state = nfcTagViewModel.state

    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        floatingActionButton = {
            LazyColumn(
                verticalArrangement = Arrangement.spacedBy(12.dp)
            ) {
                item { AddTagButton {
                    nfcTagViewModel.updateAction(NfcTagAction.Addition)
                } }
                item { UpdateTagButton {
                    nfcTagViewModel.updateAction(NfcTagAction.Updating)
                } }
            }
        },
        floatingActionButtonPosition = FabPosition.End
    ) { padding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(padding),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = if (state.nfcTag != null) state.nfcTag.toString() else "Read NFC tag",
                textAlign = TextAlign.Center,
                fontSize = 24.sp
            )
        }
    }
}
