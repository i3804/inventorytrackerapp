package com.example.inventorytracker.domain.nfc.use_case

import android.nfc.Tag
import android.os.Parcelable
import com.example.inventorytracker.common.Resource
import com.example.inventorytracker.domain.nfc.model.NfcTag
import com.example.inventorytracker.domain.nfc.repository.NfcReaderRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import javax.inject.Inject

class ReadNfc @Inject constructor(
    private val repository: NfcReaderRepository
) {
    operator fun invoke(
        action: String?,
        rawMessages: Array<Parcelable>?,
        tag: Tag?
    ): Flow<Resource<NfcTag>> = flow {
        try {
            emit(Resource.Loading<NfcTag>())

            val nfcTag = repository.readNfc(action, rawMessages, tag)
            emit(
                if (nfcTag != null) Resource.Success<NfcTag>(nfcTag)
                else Resource.Error<NfcTag>("Could not read NFC tag")
            )
        } catch (e: IOException) {
            emit(Resource.Error<NfcTag>(e.localizedMessage ?: "An unexpected error occurred"))
        }
        // finally
    }
}