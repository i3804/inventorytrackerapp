package com.example.inventorytracker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class InventoryTrackerApplication : Application()