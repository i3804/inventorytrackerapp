package com.example.inventorytracker.domain.devices.repository

import com.example.inventorytracker.common.Resource
import com.example.inventorytracker.data.devices.remote.dto.DeviceTagDto

interface GetDeviceTagsRepository {
    suspend fun getTags(): List<DeviceTagDto>
}