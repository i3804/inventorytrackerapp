package com.example.inventorytracker.presentation.main

import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.use_case.RefreshToken
import com.example.inventorytracker.domain.auth.use_case.SignOut
import com.example.inventorytracker.presentation.destinations.AuthScreenDestination
import com.example.inventorytracker.presentation.destinations.ProfileScreenDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val signOut: SignOut,
    private val refreshToken: RefreshToken,
): ViewModel() {
    private val resultChannel = Channel<AuthResult<Unit>>()
    val authResults = resultChannel.receiveAsFlow()

    init {
        authenticate()
    }

    fun onSignOut() {
        viewModelScope.launch {
            resultChannel.send(signOut())
        }
    }

    private fun authenticate() {
        viewModelScope.launch {
            val result = refreshToken()
            resultChannel.send(result)

        }
    }
}