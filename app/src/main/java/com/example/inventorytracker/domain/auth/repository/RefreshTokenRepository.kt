package com.example.inventorytracker.domain.auth.repository

import com.example.inventorytracker.domain.auth.AuthResult

interface RefreshTokenRepository {
    suspend fun refreshToken(): AuthResult<Unit>
}