package com.example.inventorytracker.domain.nfc.repository

import android.nfc.Tag
import android.os.Parcelable
import com.example.inventorytracker.domain.nfc.model.NfcTag

interface NfcReaderRepository {
    suspend fun readNfc(
        action: String?,
        rawMessages: Array<Parcelable>?,
        tag: Tag?
    ) : NfcTag?
}