package com.example.inventorytracker.data.auth.remote

import com.example.inventorytracker.domain.auth.model.AuthRequest
import com.example.inventorytracker.domain.auth.model.TokenResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthApi {
    @POST("auth/register")
    suspend fun signUp(@Body request: AuthRequest): TokenResponse

    @POST("auth/login")
    suspend fun signIn(@Body request: AuthRequest): TokenResponse

    @GET("auth/logout")
    suspend fun signOut(@Header("Authorization") token: String): Response<Unit>

    @GET("auth/refresh")
    suspend fun refreshToken(@Header("Authorization") refreshToken: String): TokenResponse
}