package com.example.inventorytracker.common

object Constants {
    const val ACCESS_TOKEN_KEY = "access_token"
    const val REFRESH_TOKEN_KEY = "refresh_token"

    const val USER_LOGIN = "user_login"
    const val USER_ROLES = "user_roles"

    const val BASE_AUTH_URL = "http://192.168.43.203:4000"
    const val BASE_DEVICE_URL = "http://192.168.43.203:3000"

    const val APP_SHARED_PREF_NAME = "app_prefs"
}