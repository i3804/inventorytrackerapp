package com.example.inventorytracker.presentation.devices

import com.example.inventorytracker.domain.devices.model.DeviceTag

data class DeviceTagsState(
    val isLoading: Boolean = false,
    val tags: List<DeviceTag> = emptyList(),
    val error: String = ""
)