package com.example.inventorytracker.di

import android.content.SharedPreferences
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.data.devices.remote.DeviceApi
import com.example.inventorytracker.data.devices.repository.GetDeviceTagsRepositoryImpl
import com.example.inventorytracker.domain.devices.repository.GetDeviceTagsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DeviceModule {
    @Provides
    @Singleton
    fun provideDeviceApi(): DeviceApi = Retrofit.Builder()
        .baseUrl(Constants.BASE_DEVICE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create()

    @Provides
    @Singleton
    fun provideGetDeviceRepository(
        api: DeviceApi,
        prefs: SharedPreferences
    ): GetDeviceTagsRepository = GetDeviceTagsRepositoryImpl(api, prefs)
}