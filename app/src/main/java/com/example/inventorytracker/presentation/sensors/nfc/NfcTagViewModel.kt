package com.example.inventorytracker.presentation.sensors.nfc

import android.nfc.Tag
import android.os.Parcelable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.inventorytracker.common.Resource
import com.example.inventorytracker.domain.nfc.model.NfcTag
import com.example.inventorytracker.domain.nfc.use_case.ReadNfc
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NfcTagViewModel @Inject constructor(
    private val readNfc: ReadNfc,
) : ViewModel() {

    var state by mutableStateOf(NfcTagState())
        private set

    var action by mutableStateOf(NfcTagAction.Reading)
        private set

    fun updateState(intentAction: String, rawMessages: Array<Parcelable>?, tag: Tag?) {
        viewModelScope.launch {
            val nfcTag = when (action) {
                NfcTagAction.Reading -> readNfc(intentAction, rawMessages, tag)
                NfcTagAction.Addition -> TODO("NFC tag addition use case!")
                NfcTagAction.Updating -> TODO("NFC tag updating use case!")
            }

            nfcTag.collect { resource: Resource<NfcTag> ->
                state = when(resource) {
                    is Resource.Error -> NfcTagState(error = resource.message ?: "An unexpected error occurred")
                    is Resource.Loading -> NfcTagState(error = resource.message ?: "Loading data")
                    is Resource.Success -> NfcTagState(nfcTag = resource.data)
                }
            }
        }
    }

    fun updateAction(_action: NfcTagAction) {
        viewModelScope.launch {
            action = _action
        }
    }
}