package com.example.inventorytracker.presentation.profile

data class ProfileState(
    val login: String = "",
    val roles: Set<String> = emptySet()
)
