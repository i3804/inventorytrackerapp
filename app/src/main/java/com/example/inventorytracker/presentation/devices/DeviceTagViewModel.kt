package com.example.inventorytracker.presentation.devices

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.inventorytracker.common.Resource
import com.example.inventorytracker.domain.auth.use_case.RefreshToken
import com.example.inventorytracker.domain.devices.model.DeviceTag
import com.example.inventorytracker.domain.devices.use_case.GetDeviceTags
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.math.BigInteger
import java.security.MessageDigest
import javax.inject.Inject

@HiltViewModel
class DeviceTagViewModel @Inject constructor(
    private val getDeviceTags: GetDeviceTags,
    private val refreshToken: RefreshToken
): ViewModel() {
    var state by mutableStateOf(DeviceTagsState())
        private set

//    private var job: Job?

    init {
        viewModelScope.launch {
            while (true) {
                state = DeviceTagsState(tags = getDeviceTagsFun())
                delay(5000)
            }
        }
    }

    private fun getTags() = getDeviceTags().onEach { resource ->
        state = when (resource) {
            is Resource.Error -> DeviceTagsState(tags = resource.data ?: emptyList())
            is Resource.Loading -> DeviceTagsState(isLoading = true)
            is Resource.Success -> DeviceTagsState(error = resource.message ?: "An unexpected error occurred")
        }

        Log.d("AuthDevices", "state: ${state.tags.joinToString(", ")}")
    }

    override fun onCleared() {
        super.onCleared()

//        job?.cancel()
//        job = null

        Log.d("AuthDevices", "Job stopped")
    }
}

fun getDeviceTagsFun(): List<DeviceTag> {
    val list = mutableListOf(
        DeviceTag(
            absence = (0..1).random(),
            active = (0..1).random(),
            deviceType = (0..2).random(),
            hash = md5("hello"),
            tookAt = "null"
        )
    )

    for (i in (1..8)) {
        list.add(
            DeviceTag(
                absence = (0..1).random(),
                active = (0..1).random(),
                deviceType = (0..2).random(),
                hash = md5("hello$i"),
                tookAt = "null"
            )
        )
    }

    return list
}

fun md5(input:String): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
}