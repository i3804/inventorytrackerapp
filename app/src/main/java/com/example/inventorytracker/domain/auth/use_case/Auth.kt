package com.example.inventorytracker.domain.auth.use_case

import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.repository.AuthRepository
import javax.inject.Inject

class Auth @Inject constructor(
    private val repository: AuthRepository
): BaseAuth() {
    suspend fun signIn(login: String, password: String): AuthResult<Unit>
        = requestWrapper { repository.signIn(login, password) }

    suspend fun signUp(login: String, password: String): AuthResult<Unit>
        = requestWrapper { repository.signUp(login, password) }
}