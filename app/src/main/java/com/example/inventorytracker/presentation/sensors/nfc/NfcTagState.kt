package com.example.inventorytracker.presentation.sensors.nfc

import com.example.inventorytracker.domain.nfc.model.NfcTag

data class NfcTagState(
    val nfcTag: NfcTag? = null,
    val error: String = "",
)
