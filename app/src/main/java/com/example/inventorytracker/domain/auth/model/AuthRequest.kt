package com.example.inventorytracker.domain.auth.model

data class AuthRequest(
    val login: String,
    val password: String
)
