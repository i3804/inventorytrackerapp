package com.example.inventorytracker.domain.auth.repository

import com.example.inventorytracker.domain.auth.AuthResult

interface AuthRepository {
    suspend fun signUp(login: String, password: String): AuthResult<Unit>
    suspend fun signIn(login: String, password: String): AuthResult<Unit>
}