package com.example.inventorytracker.data.auth.repository

import android.content.SharedPreferences
import android.util.Log
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.data.auth.remote.AuthApi
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.model.AuthRequest
import com.example.inventorytracker.domain.auth.repository.AuthRepository
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val api: AuthApi,
    private val prefs: SharedPreferences
): AuthRepository {
    override suspend fun signUp(login: String, password: String): AuthResult<Unit> {
        val response = api.signUp(
            request = AuthRequest(
                login = login,
                password = password
            )
        )

        prefs.edit().apply {
            putString(Constants.ACCESS_TOKEN_KEY, response.accessToken)
            putString(Constants.REFRESH_TOKEN_KEY, response.refreshToken)
            apply()
        }

        Log.d("Auth", "Success - signed up")

        return AuthResult.Authorized()
    }

    override suspend fun signIn(login: String, password: String): AuthResult<Unit> {
        Log.d("AuthPrefsBefore", "Access token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)} | Refresh token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)}")
        val response = api.signIn(
            request = AuthRequest(
                login = login,
                password = password
            )
        )

        prefs.edit().apply {
            putString(Constants.ACCESS_TOKEN_KEY, response.accessToken)
            putString(Constants.REFRESH_TOKEN_KEY, response.refreshToken)
            apply()
        }

        Log.d("Auth", "Success - signed in")
        Log.d("AuthPrefsAfter", "Access token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)} | Refresh token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)}")

        return AuthResult.Authorized()
    }
}