package com.example.inventorytracker.presentation.main

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.presentation.destinations.*
import com.example.inventorytracker.presentation.devices.DevicesScreen
import com.example.inventorytracker.presentation.profile.ProfileScreen
import com.example.inventorytracker.presentation.sensors.nfc.NfcScreen
import com.google.accompanist.pager.*
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.launch
import androidx.compose.runtime.setValue
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment

@OptIn(ExperimentalMaterial3Api::class, ExperimentalPagerApi::class)
@Composable
@Destination(start = true)
fun MainScreen(
    navigator: DestinationsNavigator,
    viewModel: MainViewModel = hiltViewModel()
) {
    var isLoading by rememberSaveable{ mutableStateOf(true) }
    val tabs = mapOf<NavigationItem, @Composable () -> Unit>(
        NavigationItem.Devices to { DevicesScreen() },
        NavigationItem.Nfc to { NfcScreen() },
        NavigationItem.Profile to { ProfileScreen(navigator = navigator) }
    )
    val pagerState = rememberPagerState(initialPage = 0)

    LaunchedEffect(viewModel, LocalContext.current) {
        viewModel.authResults.collect { result ->
            if (result is AuthResult.Unauthorized) {
                navigator.navigate(AuthScreenDestination) {
                    popUpTo(MainScreenDestination.route) {
                        inclusive = true
                    }
                }
            } else {
                isLoading = false
            }
        }
    }

    Scaffold { paddingValues ->
        Column {
            if (isLoading) {
                Box(
                    modifier = Modifier
                        .fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator(
                        color = MaterialTheme.colorScheme.primary
                    )
                }
            } else {
                Tabs(tabs = tabs.keys.toList(), pagerState = pagerState)
                TabsContent(tabs = tabs, pagerState = pagerState)
            }
        }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun Tabs(tabs: List<NavigationItem>, pagerState: PagerState) {
    val scope = rememberCoroutineScope()

    androidx.compose.material.TabRow(
        selectedTabIndex = pagerState.currentPage,
        backgroundColor = MaterialTheme.colorScheme.primary,
        contentColor = Color.White,
        indicator = { tabPositions ->
            TabRowDefaults.Indicator(
                Modifier.pagerTabIndicatorOffset(
                    pagerState = pagerState,
                    tabPositions = tabPositions
                )
            )
        },
    ) {
        tabs.forEachIndexed { index, tab ->
            Tab(
                selected = pagerState.currentPage == index,
                onClick = { scope.launch { pagerState.animateScrollToPage(index) } },
                text = { Text(
                    text = tab.title,
                    fontSize = 14.sp,
                ) },
                selectedContentColor = Color.White,
                unselectedContentColor = Color.White.copy(0.4f)
            )
        }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun TabsContent(tabs: Map<NavigationItem, @Composable () -> Unit>, pagerState: PagerState) {
    HorizontalPager(count = tabs.size, state = pagerState) { page ->
        tabs.values.toList()[page]()
    }
}
