package com.example.inventorytracker.presentation.auth

data class AuthState(
    val isLoading: Boolean = true,
    val login: String = "",
    val password: String = ""
)
