package com.example.inventorytracker.domain.auth.use_case

import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.repository.RefreshTokenRepository
import javax.inject.Inject

class RefreshToken @Inject constructor(
    private val repository: RefreshTokenRepository
): BaseAuth() {
    suspend operator fun invoke(): AuthResult<Unit>
        = requestWrapper { repository.refreshToken() }
}