package com.example.inventorytracker.presentation.auth

sealed class AuthUiEvent {
    data class LoginChanged(val value: String): AuthUiEvent()
    data class PasswordChanged(val value: String): AuthUiEvent()
    object SignUp: AuthUiEvent()
    object SignIn: AuthUiEvent()
}
