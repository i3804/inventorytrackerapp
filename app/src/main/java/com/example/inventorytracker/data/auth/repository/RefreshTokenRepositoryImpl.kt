package com.example.inventorytracker.data.auth.repository

import android.content.SharedPreferences
import android.util.Log
import com.auth0.android.jwt.JWT
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.data.auth.remote.AuthApi
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.repository.RefreshTokenRepository
import javax.inject.Inject

class RefreshTokenRepositoryImpl @Inject constructor(
    private val api: AuthApi,
    private val prefs: SharedPreferences
): RefreshTokenRepository {
    override suspend fun refreshToken(): AuthResult<Unit> {
        val refreshToken = prefs.getString(Constants.REFRESH_TOKEN_KEY, null)
        Log.d("AuthPrefsBefore", "Access token: ${prefs.getString(Constants.ACCESS_TOKEN_KEY, null)} | Refresh token: $refreshToken")

        if (refreshToken.isNullOrBlank()) { return AuthResult.Unauthorized() }

        val response = api.refreshToken("Bearer $refreshToken")

        Log.d("AuthRefreshToken", "Response: $response")

        if (response.accessToken == null) return AuthResult.Unauthorized()

        val jwt = JWT(refreshToken).claims
        val login = jwt["login"]?.asString()
        val roles = jwt["roles"]?.asArray(String::class.java)?.toSet()

        Log.d("AuthRefreshToken", "Success (login: ${login.toString()} | roles: ${roles})")
        prefs.edit()
            .putString(Constants.ACCESS_TOKEN_KEY, response.accessToken)
            .apply()

        prefs.edit().apply {
            putString(Constants.ACCESS_TOKEN_KEY, response.accessToken)
            putString(Constants.USER_LOGIN, login)
            putStringSet(Constants.USER_ROLES, roles)
            apply()
        }
        Log.d("AuthPrefsAfter", "Access token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)} | Refresh token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)}")

        return AuthResult.Authorized()
    }
}