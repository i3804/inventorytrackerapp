package com.example.inventorytracker.presentation.auth.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.example.inventorytracker.R
import com.example.inventorytracker.presentation.auth.AuthUiEvent
import com.example.inventorytracker.presentation.auth.AuthViewModel

@Composable
fun AuthForm(
    viewModel: AuthViewModel,
    authUiEvent: AuthUiEvent
) {
    val state = viewModel.state
    var passwordVisible by rememberSaveable {mutableStateOf(false)}

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(20.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TextField(
            value = state.login,
            onValueChange = {
                viewModel.onEvent(AuthUiEvent.LoginChanged(it))
            },
            modifier = Modifier.fillMaxWidth(),
            placeholder = {
                Text(
                    text = stringResource(id = R.string.login_placeholder)
                )
            },
            singleLine = true
        )
        Spacer(modifier = Modifier.height(16.dp))

        TextField(
            value = state.password,
            onValueChange = {
                viewModel.onEvent(AuthUiEvent.PasswordChanged(it))
            },
            modifier = Modifier.fillMaxWidth(),
            placeholder = {
                Text(
                    text = stringResource(id = R.string.password_placeholder)
                )
            },
            singleLine = true,
            visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image = if (passwordVisible)
                    Icons.Filled.Visibility
                else Icons.Filled.VisibilityOff

                // Please provide localized description for accessibility services
                val description = if (passwordVisible) "Hide password" else "Show password"

                IconButton(onClick = {passwordVisible = !passwordVisible}){
                    Icon(imageVector  = image, description)
                }
            }
        )
        Spacer(modifier = Modifier.height(16.dp))

        Button(
            onClick = { viewModel.onEvent(authUiEvent) },
            modifier = Modifier.align(Alignment.End)
        ) {
            Text(
                text = stringResource(
                    id = if (authUiEvent is AuthUiEvent.SignIn) R.string.btn_sign_in
                        else R.string.btn_sign_up
                )
            )
        }
    }
}