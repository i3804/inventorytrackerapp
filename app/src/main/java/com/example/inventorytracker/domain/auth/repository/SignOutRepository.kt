package com.example.inventorytracker.domain.auth.repository

import com.example.inventorytracker.domain.auth.AuthResult

interface SignOutRepository {
    suspend fun signOut(): AuthResult<Unit>
}