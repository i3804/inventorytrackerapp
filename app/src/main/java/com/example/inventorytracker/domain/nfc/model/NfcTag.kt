package com.example.inventorytracker.domain.nfc.model

import android.nfc.Tag

data class NfcTag(
    val tag: Tag?,
    val data: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NfcTag

        return tag == other.tag
    }

    override fun hashCode(): Int {
        var result = tag?.hashCode() ?: 0
        result = 31 * result + data.contentHashCode()
        return result
    }
}
