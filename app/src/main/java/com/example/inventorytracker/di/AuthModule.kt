package com.example.inventorytracker.di

import android.content.SharedPreferences
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.data.auth.remote.AuthApi
import com.example.inventorytracker.data.auth.repository.AuthRepositoryImpl
import com.example.inventorytracker.data.auth.repository.RefreshTokenRepositoryImpl
import com.example.inventorytracker.data.auth.repository.SignOutRepositoryImpl
import com.example.inventorytracker.domain.auth.repository.AuthRepository
import com.example.inventorytracker.domain.auth.repository.RefreshTokenRepository
import com.example.inventorytracker.domain.auth.repository.SignOutRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AuthModule {
    @Provides
    @Singleton
    fun provideAuthApi(): AuthApi = Retrofit.Builder()
        .baseUrl(Constants.BASE_AUTH_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create()

    @Provides
    @Singleton
    fun provideAuthRepository(
        api: AuthApi,
        prefs: SharedPreferences
    ): AuthRepository = AuthRepositoryImpl(api, prefs)

    @Provides
    @Singleton
    fun provideRefreshTokenRepository(
        api: AuthApi,
        prefs: SharedPreferences
    ): RefreshTokenRepository = RefreshTokenRepositoryImpl(api, prefs)

    @Provides
    @Singleton
    fun provideSignOutRepository(
        api: AuthApi,
        prefs: SharedPreferences
    ): SignOutRepository = SignOutRepositoryImpl(api, prefs)
}