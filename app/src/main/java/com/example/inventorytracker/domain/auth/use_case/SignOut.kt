package com.example.inventorytracker.domain.auth.use_case

import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.repository.SignOutRepository
import javax.inject.Inject

class SignOut @Inject constructor(
    private val repository: SignOutRepository
): BaseAuth() {
    suspend operator fun invoke(): AuthResult<Unit>
        = requestWrapper { repository.signOut() }
}