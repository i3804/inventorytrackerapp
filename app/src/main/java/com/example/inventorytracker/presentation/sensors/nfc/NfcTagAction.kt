package com.example.inventorytracker.presentation.sensors.nfc

enum class NfcTagAction {
    Reading,
    Addition,
    Updating
}