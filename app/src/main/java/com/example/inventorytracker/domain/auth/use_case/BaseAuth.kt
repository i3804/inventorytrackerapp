package com.example.inventorytracker.domain.auth.use_case

import android.util.Log
import com.example.inventorytracker.domain.auth.AuthResult
import retrofit2.HttpException

open class BaseAuth {
    protected inline fun requestWrapper(
        requestFun: () -> AuthResult<Unit>
    ): AuthResult<Unit> {
        return try {
            requestFun()
        } catch(e: HttpException) {
            Log.d("AuthHttpException", e.localizedMessage)
            if(e.code() == 401 || e.code() == 406) {
                AuthResult.Unauthorized()
            } else {
                AuthResult.UnknownError()
            }
        } catch (e: Exception) {
            Log.d("AuthException", e.localizedMessage)
            AuthResult.UnknownError()
        }
    }
}