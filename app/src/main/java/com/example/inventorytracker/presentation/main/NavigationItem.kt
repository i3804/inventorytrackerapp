package com.example.inventorytracker.presentation.main

import com.example.inventorytracker.R
import com.example.inventorytracker.presentation.destinations.DevicesScreenDestination
import com.example.inventorytracker.presentation.destinations.NfcScreenDestination

sealed class NavigationItem(var route: String, var icon: Int, var title: String) {
    object Nfc : NavigationItem(NfcScreenDestination.route, 0, "Sensors")
    object Devices : NavigationItem(DevicesScreenDestination.route, 0, "Devices")
    object Profile : NavigationItem("profile", 0, "Profile")
}
