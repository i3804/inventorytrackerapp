package com.example.inventorytracker.data.auth.repository

import android.content.SharedPreferences
import android.util.Log
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.data.auth.remote.AuthApi
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.repository.SignOutRepository
import javax.inject.Inject

class SignOutRepositoryImpl @Inject constructor(
    private val api: AuthApi,
    private val prefs: SharedPreferences
): SignOutRepository {
    override suspend fun signOut(): AuthResult<Unit> {
        val accessToken = prefs.getString(Constants.ACCESS_TOKEN_KEY, null) ?: return AuthResult.Unauthorized()
        Log.d("AuthPrefsBefore", "Access token: $accessToken | Refresh token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)}")

        val response = api.signOut(token = "Bearer $accessToken")
        Log.d("AuthSignOut", response.toString())

        prefs.edit().apply {
            remove(Constants.ACCESS_TOKEN_KEY)
            remove(Constants.REFRESH_TOKEN_KEY)
            apply()
        }
        Log.d("AuthPrefsAfter", "Access token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)} | Refresh token: ${prefs.getString(Constants.REFRESH_TOKEN_KEY, null)}")

        return AuthResult.Unauthorized()
    }
}