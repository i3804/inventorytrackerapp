package com.example.inventorytracker.data.nfc.repository

import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Parcelable
import com.example.inventorytracker.domain.nfc.model.NfcTag
import com.example.inventorytracker.domain.nfc.repository.NfcReaderRepository

class NfcReaderRepositoryImpl : NfcReaderRepository {
    override suspend fun readNfc(action: String?, rawMessages: Array<Parcelable>?, tag: Tag?): NfcTag? {
        when (action) {
            NfcAdapter.ACTION_NDEF_DISCOVERED -> {
                rawMessages?.let {
                    return parseNdefMessage(
                        it.map { msg -> msg as NdefMessage }.toTypedArray(),
                        tag
                    )
                }
            }
            NfcAdapter.ACTION_TECH_DISCOVERED -> {

            }
            NfcAdapter.ACTION_TAG_DISCOVERED -> {

            }
            else -> {}
        }

        return null
    }

    private fun parseNdefMessage(messages: Array<NdefMessage>, tag: Tag?): NfcTag {
        // TODO: rewrite it
        if (messages.isNotEmpty()) {
            messages[0].let { message ->
                val data = Array(message.records.size) { index ->
                    val toIndex = message.records[index].payload.size
                    message.records[index].payload
                        .asList().subList(3, toIndex)
                        .joinToString(separator = "") { byte ->
                            byte.toInt().toChar().toString()
                        }
                }

                return NfcTag(tag, data = data)
            }
        }

        return NfcTag(tag, emptyArray())
    }
}