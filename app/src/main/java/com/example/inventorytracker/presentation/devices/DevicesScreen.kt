package com.example.inventorytracker.presentation.devices

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.inventorytracker.domain.devices.model.DeviceTag
import com.ramcosta.composedestinations.annotation.Destination

@Composable
@Destination
fun DevicesScreen(
    viewModel: DeviceTagViewModel = hiltViewModel()
) {
    val state = viewModel.state

    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(state.tags) { tag ->
                TagListItem(tag)
                Divider()
            }
        }

        if (state.error.isNotBlank()) {
            Text(
                text = state.error,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .align(Alignment.Center)
            )
        }
        if (state.isLoading) {
            CircularProgressIndicator(
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }
}

@Composable
fun TagListItem(
    tag: DeviceTag,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column {
            Text(
                text = "${tag.hash.slice(0..6)} (${if (tag.absence == 0) "present" else "absent"})",
                fontSize = 20.sp
            )
            Text(
                text = "Took at: ${tag.tookAt}",
                fontSize = 14.sp,
                fontStyle = FontStyle.Italic
            )
        }

        Box(
            modifier = Modifier
                .size(16.dp)
                .clip(CircleShape)
                .background(
                    if (tag.active == 0) Color.Red else Color.Green
                )
        )
    }
}

@Preview(showBackground = true)
@Composable
fun TagListItemPreview() {
    val tag = DeviceTag(
        0, 1, 1, "sdfsdfsdfsefsdfsesdfsdfse", "20-12-2000"
    )
    
    TagListItem(tag = tag)
}