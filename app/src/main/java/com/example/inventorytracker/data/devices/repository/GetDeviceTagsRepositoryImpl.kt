package com.example.inventorytracker.data.devices.repository

import android.content.SharedPreferences
import android.util.Log
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.common.Resource
import com.example.inventorytracker.data.devices.remote.DeviceApi
import com.example.inventorytracker.data.devices.remote.dto.DeviceTagDto
import com.example.inventorytracker.domain.devices.repository.GetDeviceTagsRepository
import javax.inject.Inject

class GetDeviceTagsRepositoryImpl @Inject constructor(
    private val api: DeviceApi,
    private val prefs: SharedPreferences
): GetDeviceTagsRepository {
    override suspend fun getTags(): List<DeviceTagDto> {
        val accessToken = prefs.getString(Constants.ACCESS_TOKEN_KEY, null)
            ?: return emptyList()
        Log.d(
            "AuthPrefsBefore", "Access token: $accessToken | Refresh token: ${
                prefs.getString(
                    Constants.REFRESH_TOKEN_KEY, null
                )
            }"
        )

        return api.getTags(accessToken = "Bearer $accessToken")
    }
}