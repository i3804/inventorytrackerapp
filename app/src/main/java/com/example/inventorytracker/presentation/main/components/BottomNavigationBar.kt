package com.example.inventorytracker.presentation.main.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.example.inventorytracker.presentation.destinations.DevicesScreenDestination
import com.example.inventorytracker.presentation.destinations.DirectionDestination
import com.example.inventorytracker.presentation.destinations.MainScreenDestination
import com.example.inventorytracker.presentation.destinations.NfcScreenDestination
import com.example.inventorytracker.presentation.main.NavigationItem
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.launch

@Composable
fun BottomNavigationBar(
    navigator: DestinationsNavigator
) {
//    val tabs = mapOf(
//        NavigationItem.Devices to DevicesScreenDestination,
//        NavigationItem.Nfc to NfcScreenDestination,
//        NavigationItem.Profile to ProfileScreenDestination
//    )

//    val pagerState = rememberPagerState(initialPage = 0)
//
//    TabRow(
//        selectedTabIndex = 0
//    ) {
//        tabs.forEachIndexed { index, tab ->
//            Tab(
//                selected = tab is NavigationItem.Devices,
//
//            )
//        }
//    }

//    val scope = rememberCoroutineScope()
//
//    BottomAppBar(
//        contentColor = Color.White,
//        containerColor = MaterialTheme.colorScheme.primary,
//    ) {
//        tabs.forEach { tab ->
//            Tab(
//                selected = tab is NavigationItem.Devices,
//                onClick = {
//                    scope.launch {
//                        navigator.navigate(tab.route) {
//                            popUpTo(MainScreenDestination.route) {
//                                saveState = true
//                            }
//
//                            restoreState = true
//                            launchSingleTop = true
//                        }
//                    }
//                },
//                modifier = Modifier
//                    .weight(1f)
//                    .fillMaxHeight(),
//                selectedContentColor = Color.White,
//                unselectedContentColor = Color.White.copy(0.4f),
//            ) {
//                Text(text = tab.title)
//            }
//        }
//    }
}