package com.example.inventorytracker.data.devices.remote

import com.example.inventorytracker.data.devices.remote.dto.DeviceTagDto
import com.example.inventorytracker.domain.devices.model.DeviceTagRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface DeviceApi {
    @GET("tag/fetch")
    suspend fun getTags(@Header("Authorization") accessToken: String): List<DeviceTagDto>

    @POST("tag/add")
    suspend fun addTag(
        @Header("Authorization") accessToken: String,
        @Body request: DeviceTagRequest
    )
}