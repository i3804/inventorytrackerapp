package com.example.inventorytracker.domain.devices.model

import com.google.gson.annotations.SerializedName

data class DeviceTagRequest(
    val hash: String,
    @SerializedName("device_type")
    val deviceType: Int
)
