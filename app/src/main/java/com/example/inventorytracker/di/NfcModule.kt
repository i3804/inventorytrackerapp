package com.example.inventorytracker.di

import com.example.inventorytracker.data.nfc.repository.NfcReaderRepositoryImpl
import com.example.inventorytracker.domain.nfc.repository.NfcReaderRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NfcModule {
    @Provides
    @Singleton
    fun provideNfcReaderRepository(): NfcReaderRepository = NfcReaderRepositoryImpl()
}