package com.example.inventorytracker.domain.auth.model

data class TokenResponse(
    val accessToken: String?,
    val refreshToken: String
)
