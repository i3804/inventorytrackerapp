package com.example.inventorytracker.presentation.auth

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.use_case.Auth
import com.example.inventorytracker.domain.auth.use_case.RefreshToken
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val auth: Auth,
    private val refreshToken: RefreshToken
): ViewModel() {
    var state by mutableStateOf(AuthState())
        private set

    private val resultChannel = Channel<AuthResult<Unit>>()
    val authResults = resultChannel.receiveAsFlow()

    init {
        authenticate()
    }

    fun onEvent(event: AuthUiEvent) {
        when(event) {
            is AuthUiEvent.LoginChanged -> state = state.copy(login = event.value)
            is AuthUiEvent.PasswordChanged -> state = state.copy(password = event.value)
            is AuthUiEvent.SignIn -> signIn()
            is AuthUiEvent.SignUp -> signUp()
        }
    }

    private fun signUp() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = auth.signUp(
                login = state.login.trim(),
                password = state.password
            )
            resultChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun signIn() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = auth.signIn(
                login = state.login.trim(),
                password = state.password
            )
            resultChannel.send(result)

            state = state.copy(isLoading = false)
        }
    }

    private fun authenticate() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = refreshToken()
            resultChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }
}
