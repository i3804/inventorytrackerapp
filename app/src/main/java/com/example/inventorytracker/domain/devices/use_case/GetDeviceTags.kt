package com.example.inventorytracker.domain.devices.use_case

import com.example.inventorytracker.common.Resource
import com.example.inventorytracker.data.devices.remote.dto.toDeviceTag
import com.example.inventorytracker.domain.devices.model.DeviceTag
import com.example.inventorytracker.domain.devices.repository.GetDeviceTagsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetDeviceTags @Inject constructor(
    private val repository: GetDeviceTagsRepository
) {
    operator fun invoke(): Flow<Resource<List<DeviceTag>>> = flow {
        try {
            emit(Resource.Loading())
            val tags = repository.getTags().map { it.toDeviceTag() }
            emit(Resource.Success(tags))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occurred"))
        } catch (e: IOException) {
            emit(Resource.Error(e.localizedMessage ?: "No Internet access"))
        }
    }
}