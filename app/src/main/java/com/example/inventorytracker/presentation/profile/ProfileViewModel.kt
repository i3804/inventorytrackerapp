package com.example.inventorytracker.presentation.profile

import android.content.SharedPreferences
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.inventorytracker.common.Constants
import com.example.inventorytracker.domain.auth.AuthResult
import com.example.inventorytracker.domain.auth.use_case.RefreshToken
import com.example.inventorytracker.domain.auth.use_case.SignOut
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val signOut: SignOut,
    private val refreshToken: RefreshToken,
    private val prefs: SharedPreferences
): ViewModel() {

    var state by mutableStateOf(ProfileState())
        private set

    private val resultChannel = Channel<AuthResult<Unit>>()
    val authResults = resultChannel.receiveAsFlow()

    init {
        val login = prefs.getString(Constants.USER_LOGIN, null)
        val roles = prefs.getStringSet(Constants.USER_ROLES, null)

        if (login == null || roles == null) {
            Log.d("AuthProfile", "login: $login | roles: $roles")
        } else {
            state = ProfileState(
                login = login,
                roles = roles
            )
        }
    }

    fun onSignOut() {
        viewModelScope.launch {
            resultChannel.send(signOut())
        }
    }
}