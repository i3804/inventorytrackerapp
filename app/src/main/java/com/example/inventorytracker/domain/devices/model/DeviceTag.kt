package com.example.inventorytracker.domain.devices.model

data class DeviceTag(
    val absence: Int,
    val active: Int,
    val deviceType: Int,
    val hash: String,
    val tookAt: String
)